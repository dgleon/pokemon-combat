# Reto Semanal #2

---

# Combate Pokémon 💥

---

## Entrega de la solución
> Se debe de realizar un `fork` al proyecto y posteriormente
> una `pull request` para dar la solución como entregada.

---

## Enunciado del Reto
>Crea un programa que calcule el daño de un ataque durante
 una batalla Pokémon

---

### Consideraciones
> - La fórmula será la siguiente: daño = 50 * (ataque / defensa) * efectividad
> - Efectividad: x2 (súper efectivo), x1 (neutral), x0.5 (no es muy efectivo)
> - Sólo hay 4 tipos de Pokémon: Agua, Fuego, Planta y Eléctrico   (buscar su efectividad)
> - El programa recibe los siguientes parámetros:
> - - Tipo del Pokémon atacante.
> - - Tipo del Pokémon defensor.
> - - Ataque: Entre 1 y 100.
> - - Defensa: Entre 1 y 100.
### Ejemplo
> Si el atacante es tipo `FUEGO`🔥 y el defensor tipo `PLANTA`🌿, teniendo como ataque un valor de `50` y como
> defensa un valor de `50`, considerando la efectividad, el daño resultante debería de ser `100`.

### Orientación para la resolución del Reto
> - El reto se puede resolver en cualquier lenguaje de programación existente (preferiblemente en *Java* o *Javascript*)
> - Se puede plataformar el proyecto como se prefiera (maven o gradle para Java, por ejemplo).
> - Al entregar la solución, se debe de poder comprobar que funciona al menos por consola o a través de tests.

`Happy Coding 🚀`